#include "pch.hpp"

#include "hamster.hpp"

#include "engine.hpp"

hamster::Hamster::Hamster()
{
  this->data = {new internal::Engine, [](void* ptr) { delete static_cast<internal::Engine*>(ptr); }};
}

hamster::Window::Window(std::size_t const width, std::size_t const height, std::string const& title)
{
  this->data = {new internal::Window{width, height, title},
                [](void* ptr) { delete static_cast<internal::Window*>(ptr); }};
}

hamster::Window hamster::Hamster::createWindow(std::size_t const width, std::size_t const height,
                                               std::string const& title)
{
  return hamster::Window(width, height, title);
};
