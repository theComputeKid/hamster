/**
 * @file constants.hpp
 *
 * @brief Compile-time constants.
 *
 * @author theComputeKid (theComputeKid@gmail.com)
 * @version 0.1
 * @date 2022-05-12
 *
 * @copyright Copyright (c) 2022 theComputeKid
 */
#pragma once

#include "pch.hpp"

namespace hamster::internal::constants
{
  std::size_t constexpr maxWidth = 1920;
  std::size_t constexpr maxHeight = 1080;
} // namespace hamster::internal
