#include "pch.hpp"

#include "hamster.hpp"

#include "constants.hpp"
#include "engine.hpp"

namespace
{
  std::vector<VkExtensionProperties> iGetAvailableVulkanExtensions()
  {
    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());
    return extensions;
  }

  void iInitVulkan(VkInstance& instance)
  {
    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "hamster";
    appInfo.applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
    appInfo.pEngineName = "hamster";
    appInfo.engineVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;

    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    createInfo.enabledExtensionCount = glfwExtensionCount;
    createInfo.ppEnabledExtensionNames = glfwExtensions;

    createInfo.enabledLayerCount = 0;

    if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS)
    {
      throw std::runtime_error("failed to create instance!");
    }
  }
} // namespace

namespace hamster
{
  internal::Window::Window(std::size_t const width, std::size_t const height, std::string const& title)
  {
    int const trueWidth = static_cast<int>(std::min(width, internal::constants::maxWidth));
    int const trueHeight = static_cast<int>(std::min(height, internal::constants::maxHeight));

    this->window = glfwCreateWindow(trueWidth, trueHeight, title.c_str(), nullptr, nullptr);

    while (!glfwWindowShouldClose(window))
    {
      glfwPollEvents();
    }
  }

  internal::Window::~Window() { glfwDestroyWindow(window); }

  internal::Engine::Engine()
  {
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    iInitVulkan(instance);
    availableVulkanExtensions = iGetAvailableVulkanExtensions();
  }

  internal::Engine::~Engine()
  {
    vkDestroyInstance(instance, nullptr);
    glfwTerminate();
  }
} // namespace hamster
