/**
 * @file engine.hpp
 *
 * @brief Internal engine API.
 *
 * @author theComputeKid (theComputeKid@gmail.com)
 * @version 0.1
 * @date 2022-05-12
 *
 * @copyright Copyright (c) 2022 theComputeKid
 */
#pragma once
#include "pch.hpp"

namespace hamster::internal
{
  struct Window
  {
    Window(std::size_t const width, std::size_t const height, std::string const& title);
    ~Window();

  private:
    GLFWwindow* window;
  };

  struct Engine
  {
    Engine();
    ~Engine();

  private:
    std::vector<VkExtensionProperties> availableVulkanExtensions;
    VkInstance instance;
  };
} // namespace hamster::internal
