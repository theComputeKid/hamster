#include "pch.hpp"

#include "constants.hpp"

static_assert(hamster::internal::constants::maxWidth < INT_MAX, "maxWidth must be less than int_max for glfw logic.");
static_assert(hamster::internal::constants::maxHeight < INT_MAX, "maxHeight must be less than int_max for glfw logic.");
