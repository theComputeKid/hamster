/**
 * @file pch.hpp
 *
 * @brief Precompiled Header file.
 *
 * @author theComputeKid (theComputeKid@gmail.com)
 * @version 0.1
 * @date 2022-05-08
 *
 * @copyright Copyright (c) 2022 theComputeKid
 */
#pragma once

#include <algorithm>
#include <climits>
#include <cstddef>
#include <execution>
#include <string>
#include <vector>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <vulkan/vulkan.h>
