/**
 * @file pch.cpp
 *
 * @brief Precompiled Source File needed for MSVC.
 *
 * @author theComputeKid (theComputeKid@gmail.com)
 * @version 0.1
 * @date 2022-05-08
 *
 * @copyright Copyright (c) 2022 theComputeKid
 */

#include "pch.hpp"
