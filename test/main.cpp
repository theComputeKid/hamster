#include "pch.hpp"

#include "hamster.hpp"

int WinMain()
{
  auto myHamster = hamster::Hamster();
  auto myWindow = myHamster.createWindow(800,620,"hamster");

  return 0;
}
