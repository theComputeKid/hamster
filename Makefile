#* MAKEFILE FOR WINDOWS
OUT_DIR 	= build
VENDOR_DIR	= vendor

PREMAKE_OUT	= $(VENDOR_DIR)\premake-core\bin\release\premake5.exe

GLFW_OUT_RELEASE	= $(VENDOR_DIR)\glfw\build\src\Release\glfw3.lib
GLFW_OUT_DEBUG		= $(VENDOR_DIR)\glfw\build\src\Debug\glfw3.lib

# Build everything rules
all: debug release
	@echo Completed Build: $@

debug: 3p
	@cd $(OUT_DIR) && msbuild -p:Configuration=$@ -m -v:Normal
	@echo Completed Build: $@

release: 3p
	@cd $(OUT_DIR) && msbuild -p:Configuration=$@ -m -v:Normal
	@echo Completed Build: $@

# 3p general build rules
3p: $(PREMAKE_OUT) $(GLFW_OUT_DEBUG) $(GLFW_OUT_RELEASE)
	$(PREMAKE_OUT) vs2022

# Rule to build Premake for first use
# (1) Change Directory to Premake SRC
# (2) Run NMAKE
# (3) Copy premake executable into the workspace root
$(PREMAKE_OUT):
	@echo Building Premake for Windows
	@cd $(VENDOR_DIR)\premake-core && $(MAKE) -nologo -f Bootstrap.mak windows && @echo Built Premake

# Rule to build GLFW for first use
$(GLFW_OUT_DEBUG):
	@cmake -S $(VENDOR_DIR)\glfw -B $(VENDOR_DIR)\glfw\build && msbuild $(VENDOR_DIR)\glfw\build\GLFW.sln -p:Configuration=Debug -m -v:Normal

$(GLFW_OUT_RELEASE):
	@cmake -S $(VENDOR_DIR)\glfw -B $(VENDOR_DIR)\glfw\build && msbuild $(VENDOR_DIR)\glfw\build\GLFW.sln -p:Configuration=Release -m -v:Normal

clean:
	@if exist $(OUT_DIR) rmdir /s /q $(OUT_DIR)

clean_3p:
	@if exist $(PREMAKE_OUT) del /s /q $(PREMAKE_OUT)
	@if exist $(VENDOR_DIR)\glfw\build rmdir /s /q $(VENDOR_DIR)\glfw\build

clean_all: clean clean_3p
	@echo Cleaning everything
