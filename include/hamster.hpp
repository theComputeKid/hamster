/**
 * @file hamster.hpp
 *
 * @brief C++ export API.
 *
 * @author theComputeKid (theComputeKid@gmail.com)
 * @version 0.1
 * @date 2022-05-08
 *
 * @copyright Copyright (c) 2022 theComputeKid
 */
#pragma once

#include <cstddef>
#include <functional>
#include <memory>
#include <string>

namespace hamster
{
  struct Window
  {
    friend struct Hamster;

  private:
    // Private constructor so only Hamster can construct it.
    Window(std::size_t const width, std::size_t const height, std::string const& title);

    // RAII data
    std::unique_ptr<void, std::function<void(void*)>> data;
  };

  struct Hamster
  {
    Hamster();

    Window createWindow(std::size_t const width, std::size_t const height, std::string const& title);

  private:
    // RAII data
    std::unique_ptr<void, std::function<void(void*)>> data;
  };
} // namespace hamster
