workspace "Hamster"

    location "build"

    language "C++"

    cdialect "C17"

    cppdialect "C++20"

    architecture "x64"

    configurations {"debug", "release"}

    includedirs { "include", "." }

    flags {
        "MultiProcessorCompile",
        "RelativeLinks",
        "Maps"
    }

    objdir("build/bin/obj/%{cfg.buildcfg}/%{prj.name}")

    targetdir("build/bin/%{cfg.buildcfg}")

    filter "configurations:release"
        flags "LinkTimeOptimization"
        symbols "Off"
        optimize "Full"
        runtime "Release"
    filter ""

    filter "configurations:debug"
        defines {"DEBUG"}
        symbols "On"
        optimize "Off"
        runtime "Debug"
    filter ""

    visibility "Hidden"

    warnings "Extra"

    startproject "gpuPlotTest"

    conformancemode "On"

    debugformat "c7"

    dpiawareness "HighPerMonitor"

    justmycode "On"

    openmp "On"

    systemversion "latest"

    vectorextensions "AVX2"

-- ?-------------------- Load Projects ----------------------

project "engine"

    kind "StaticLib"

    targetname "hamster"

    location("build/%{prj.name}")

    includedirs "vendor/glfw/include"

    if (os.target() == "windows") then
        includedirs "$(VULKAN_SDK)/include"
        libdirs "$(VULKAN_SDK)/lib"
        links "vulkan-1.lib"
    end

    files {
       "include/**",
       "src/**.cpp"
    }

    vpaths {
        ["Export/*"] = "include/**",
        ["Sources/*"] = "src/**.cpp"
    }

    pchheader "pch.hpp"
    pchsource "src/pch.cpp"

    defines {
        "HAMSTER_EXPORT"
    }

project "test"

    kind "WindowedApp"

    targetname "hamster-test"

    location("build/%{prj.name}")

    files {
       "include/**",
       "test/**.cpp"
    }

    vpaths {
        ["Import/*"] = "include/**",
        ["Sources/*"] = "test/**.cpp"
    }

    pchheader "pch.hpp"
    pchsource "test/pch.cpp"

    links {
        "engine",
        "glfw3"
    }

    filter "configurations:debug"
        libdirs "vendor/glfw/build/src/Debug"

    filter "configurations:release"
        libdirs "vendor/glfw/build/src/Release"
    filter ""

    if (os.target() == "windows") then
        libdirs "$(VULKAN_SDK)/lib"
        links "vulkan-1.lib"
    end
